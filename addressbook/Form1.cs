﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace addressbook
{
    public partial class Form1 : Form
    {

        List<Object> people = new List<Object>();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Object ob = new Object();
            ob.Surname = textBox1.Text;
            ob.PhoneNumber = textBox2.Text;
            people.Add(ob);

            people.Sort();

            foreach(Object p in people)
            {
                var o = p.Surname +" "+ p.PhoneNumber;
                listBox1.Items.Add(o);
            }

        }
    }
}
