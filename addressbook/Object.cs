﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace addressbook
{
    class Object : IComparable<Object>
    {
        public string Surname;
        public string PhoneNumber;

        public int CompareTo(Object _surname)
        {
            return this.Surname.CompareTo(_surname.Surname);
        }

    }
}
